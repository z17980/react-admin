module.exports = (req,res) => {

  let { id,name } = req.body

  let sql = `SELECT name FROM twocategories WHERE name = :name`

  global.originalQuery(sql,{name},'SELECT').then((result) => {

    if (result.length === 0) {

      let sql = `UPDATE twocategories SET name=:name WHERE cid=:id`

      global.originalQuery(sql,{id,name},'UPDATE').then((result) => {

        res.json({
    
          code:200,
    
          msg:'保存成功！'
    
        })
        
      })
      
    }
    else{

      res.json({

        code:203,

        msg:'二级分类已存在！请勿重复添加'

      })

    }
    
  })

}