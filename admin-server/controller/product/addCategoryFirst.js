module.exports = (req,res) => {

  let { name } = req.body

  let sql = `SELECT name FROM categories WHERE name = :name`

  global.originalQuery(sql,{name},'SELECT').then((result) => {

    if (result.length === 0) {
      
      let sql = `INSERT INTO categories (name) VALUES (:name)`

      global.originalQuery(sql,{name},'INSERT').then((result) => {
    
          res.json({
    
            code:200,
    
            msg:'添加一级分类成功！',
    
            data:result
    
          })
        
      })

    }
    else{

      res.json({

        code:203,

        msg:'分类已存在！请勿重复添加'

      })

    }
    
  })

}