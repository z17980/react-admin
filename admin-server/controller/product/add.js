const { log } = require("debug")

module.exports = (req,res) => {

    let { name,info,price,status } = req.body

    status = status === '在售'?1:0
  
    let sql = `SELECT name FROM products WHERE name = :name`
  
    global.originalQuery(sql,{name},'SELECT').then((result) => {
  
      if (result.length === 0) {
        
        let sql = `INSERT INTO products (name,info,price,status) VALUES (:name,:info,:price,:status)`
  
        global.originalQuery(sql,{name,info,price,status},'INSERT').then((result) => {
      
            res.json({
      
              code:200,
      
              msg:'添加商品成功！',
      
              data:result
      
            })
          
        })
  
      }
      else{
  
        res.json({
  
          code:203,
  
          msg:'商品已存在！请勿重复添加'
  
        })
  
      }
      
    })
  
  }