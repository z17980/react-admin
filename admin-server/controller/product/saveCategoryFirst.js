module.exports = (req,res) => {

  let { id,name } = req.body

  let sql = `SELECT name FROM categories WHERE name = :name`

  global.originalQuery(sql,{name},'SELECT').then((result) => {

    if (result.length === 0) {

      let sql = `UPDATE categories SET name=:name WHERE cid=:id`

      global.originalQuery(sql,{id,name},'UPDATE').then((result) => {
    
        res.json({
    
          code:200,
    
          msg:'保存成功！'
    
        })
        
      })
      
    }
    else{

      res.json({

        code:203,

        msg:'分类已存在！请勿重复添加'

      })

    }
    
  })

}