module.exports = (req,res) => {

  let { pid,name } = req.body

  console.log(pid,name);

  let sql = `SELECT name FROM twocategories WHERE name = :name`

  global.originalQuery(sql,{name},'SELECT').then((result) => {

    if (result.length === 0) {
      
      let sql = `INSERT INTO twocategories (name,pid) VALUES (:name,:pid)`

      global.originalQuery(sql,{name,pid},'INSERT').then((result) => {
    
          res.json({
    
            code:200,
    
            msg:'添加二级分类成功！',
    
            data:result
    
          })
        
      })

    }
    else{

      res.json({

        code:203,

        msg:'二级分类已存在！请勿重复添加'

      })

    }
    
  })

}