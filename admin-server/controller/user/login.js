// 用户登录

let { getUserToken } = require('../../utils/user/token')

const login = (req,res)=>{

  let { username,password } = req.body

  let sql = `SELECT 

  uid,adminname,password,status,role 
  
  FROM admins
  
  WHERE adminname = :username`

  let msg = ''

  let data = {}

  let code = 200

  let token = ''

  global.originalQuery(sql,{username},'SELECT').then((result)=>{

      if (result.length === 0) {

          code = 201,
        
          msg = '用户不存在！'

      }else if( password === result[0].password ) {

          msg = '用户登录成功！'

          token = getUserToken(result[0].password) 

          data = {

            name:result[0].adminname,

            role:result[0].role

          }

      }else{

          code = 203

          msg = '用户名或者密码错误！'

      }
      res.json({

        code,

        msg,
    
        data,

        token:token?token:''
    
      })

  })

}

module.exports = login