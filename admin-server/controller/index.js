let login = require('./user/login')

let categoryFirst = require('./product/categoryFirst')

let addCategoryFirst = require('./product/addCategoryFirst')

let addCategoryTwo = require('./product/addCategoryTwo')

let delCategoryFirst = require('./product/delCategoryFirst')

let saveCategoryFirst = require('./product/saveCategoryFirst')

let findTwoCategories = require('./product/findTwoCategories')

let saveChildCategory = require('./product/saveChildCategory')

let delChildCategory = require('./product/delChildCategory')

let add = require('./product/add')

let find = require('./product/find')

let del = require('./product/del')

let edit = require('./product/edit')

let change = require('./product/change')

let search = require('./product/search')

let findByStatus = require('./product/findByStatus')

module.exports = {

  login,

  categoryFirst,

  addCategoryFirst,

  addCategoryTwo,

  delCategoryFirst,

  saveCategoryFirst,

  findTwoCategories,

  saveChildCategory,

  delChildCategory,

  add,

  find,

  del,

  edit,

  change,

  search,

  findByStatus

}