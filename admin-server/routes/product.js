var express = require('express');

var router = express.Router();

// 品类管理
let { 
  categoryFirst,
  addCategoryFirst,
  addCategoryTwo,
  delCategoryFirst,
  saveCategoryFirst,
  findTwoCategories,
  saveChildCategory,
  delChildCategory
} = require('../controller/index') 

router.post('/categoryFirst',categoryFirst);

router.post('/addCategoryFirst',addCategoryFirst);

router.post('/addCategoryTwo',addCategoryTwo);

router.post('/delCategoryFirst',delCategoryFirst);

router.post('/saveCategoryFirst',saveCategoryFirst);

router.post('/findTwoCategories',findTwoCategories);

router.post('/saveChildCategory',saveChildCategory);

router.post('/delChildCategory',delChildCategory);

// end

// 商品管理

let { add,find,del,edit,change,search,findByStatus} = require('../controller/index') 

router.post('/add',add);

router.post('/find',find);

router.post('/del',del);

router.post('/edit',edit);

router.post('/change',change);

router.post('/search',search);

router.post('/findByStatus',findByStatus);

module.exports = router;
