const jwt = require('jsonwebtoken');

// 获取token
const getUserToken = (val)=> {

  return jwt.sign({

             data: val

         }, 

         'secret', 

         { 

             expiresIn: '1d' 
         });

}

//验证token
const varifyUserToken = (token)=>{

  return jwt.verify(token,'secret',(err,data) => {


      if(err){

          return err

      }

      else{

          return data

      }


  });

}

module.exports = {

  getUserToken,

  varifyUserToken

}