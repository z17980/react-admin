const { QueryTypes } = require('sequelize');

const asyncDb = async ()=>{

  // 同步模型
  await global.sequelize.sync({ alter: true });

}

// sql查询 
const originalQuery = (sql,params,dbType)=> {

  return sequelize.query(
       sql,
       {
         replacements: params,
         type: QueryTypes[dbType]
       }
   );

}

module.exports =  { asyncDb,originalQuery }