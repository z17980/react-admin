var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/user');
var productRouter = require('./routes/product');

// 连接数据库
let { sequelize, connectTest } = require('./config/connect')

global.sequelize = sequelize

connectTest()

// 创建模型
let { user } = require('./models/index')

// 同步模型
let { asyncDb,originalQuery } = require('./utils/sequelize/index')

asyncDb()

global.originalQuery = originalQuery

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// 处理跨域
var cors = require('cors')
app.use(cors())

// 配置请求白名单
let middle = require('./config/middle')
middle(app)

app.use('/', indexRouter);
app.use('/product',productRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
