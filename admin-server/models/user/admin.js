const { DataTypes, Model } = require('sequelize');

const sequelize = global.sequelize

class Admin extends Model {}

Admin.init({
  uid: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    comment: '用户id'
  },
  adminname: {
    type: DataTypes.STRING,
    allowNull: false,
    comment: '用户名'
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
    comment: '密码'
  },
  status: {
    type: DataTypes.INTEGER,
    allowNull: false,
    // 0:禁用
    // 1:启用
    defaultValue:1,
    comment: '用户状态'
  },
  role: {
    type: DataTypes.INTEGER,
    allowNull: false,
    defaultValue:1,
    comment: '用户权限'
  }
},{
  sequelize,
  createdAt:false,
  updatedAt:false,
  modelName: 'Admin'
});

module.exports = Admin
