const { DataTypes, Model } = require('sequelize');

const sequelize = global.sequelize

class product extends Model {}

product.init({
  pid: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    comment: '商品id'
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
    comment: '商品名'
  },
  info: {
    type: DataTypes.STRING,
    allowNull: false,
    comment: '商品描述'
  },
  status: {
    type: DataTypes.INTEGER,
    allowNull: false,
    // 0:禁用
    // 1:启用
    defaultValue:1,
    comment: '商品状态'
  },
  price: {
    type: DataTypes.FLOAT,
    allowNull: false,
    defaultValue:1,
    comment: '价格'
  }
},{
  sequelize,
  createdAt:false,
  updatedAt:false,
  modelName: 'product'
});

module.exports = product
