const { DataTypes, Model } = require('sequelize');

const sequelize = global.sequelize

class Category extends Model {}

Category.init({
  cid: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
    comment: '一级分类id'
  },
  pid: {
    type: DataTypes.INTEGER,
    allowNull: false,
    defaultValue:0,
    comment: '父级分类id'
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
    comment: '分类名'
  },
  status: {
    type: DataTypes.INTEGER,
    allowNull: false,
    // 0:禁用
    // 1:启用
    defaultValue:1,
    comment: '用户状态'
  },
  role: {
    type: DataTypes.INTEGER,
    allowNull: false,
    defaultValue:1,
    comment: '用户权限'
  }
},{
  sequelize,
  createdAt:false,
  updatedAt:false,
  modelName: 'Category'
});

module.exports = Category
