let admin = require('./user/admin')

let category = require('./product/category')

let twoCategories = require('./product/twoCategories')

let product = require('./product/product')

module.exports = {

  admin,

  category,

  twoCategories,

  product

}