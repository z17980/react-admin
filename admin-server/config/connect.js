const { Sequelize } = require('sequelize');

// 方法 2: 分别传递参数 (其它数据库)
const sequelize = new Sequelize('react-admin', 'root', '123456', {
  host: 'localhost',
  dialect:'mysql' /* 选择 'mysql' | 'mariadb' | 'postgres' | 'mssql' 其一 */
});

// 连接测试
const connectTest= async ()=>{

  try {
    await sequelize.authenticate();
    console.log('数据库连接成功！');
  } catch (error) {
    console.error('数据库连接失败！', error);
  }

}

module.exports = {

  sequelize,

  connectTest

}
