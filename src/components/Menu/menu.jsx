import React, { Component } from 'react'

import { withRouter } from 'react-router-dom'

import {

  Menu,

  createFromIconfontCN,

} from '@/plugins/antd'

import { Link } from 'react-router-dom';

const { SubMenu } = Menu;

const IconFont = createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_2717384_71hbwvx8xru.js',
});

class menu extends Component {
  
  state={

    pathname:null,

    menuList:[
      {
          title: '首页',
          key: '/index',
          icon: 'icon-zhuye',
      },{
          title: '商品',
          key: '/products',
          icon: 'icon-shangpin',
          children: [
              {
                  title: '品类管理',
                  key: '/category',
                  icon: 'icon-leimupinleifenleileibie'
              },{
                  title: '商品管理',
                  key: '/product',
                  icon: 'icon-shangpin'
          }]
      },{
          title: '用户管理',
          key: '/user',
          icon: 'icon-yonghu'
      },{
          title: '角色管理',
          key: '/role',
          icon: 'icon-jiaose'
      },{
          title: '图形图表',
          key: '/charts',
          icon: 'icon-tubiao1',
          children: [
              {
                  title: '柱形图',
                  key: '/charts/bar',
                  icon: 'icon-tubiaozhuxingtu'
              }, {
                  title: '折线图',
                  key: '/charts/line',
                  icon: 'icon-tubiao'
              }, {
                  title: '饼图',
                  key: '/charts/pie',
                  icon: 'icon-tubiao1'
          }]
      }
    ]

  }

  getMenuNodes = (menuList,pathname) => {

    return menuList.map(item =>{

        if(!item.children){

            return (
          
                <Menu.Item onClick = {()=>{this.menuClick(item)}}  key={item.key} icon={<IconFont type={item.icon}/>}>
                    <Link to={item.key} >
                        {item.title}
                    </Link>
                </Menu.Item>
            )

        }else{

            let child = item.children.find(val=> val.key === pathname)

            if (child) {

              this.openKey = item.key

            }

            return (
                <SubMenu icon={<IconFont type={item.icon}/>} key={item.key} title={item.title}>
                    {this.getMenuNodes(item.children)}
                </SubMenu>
            )
        }
    })

  }

  findHeadPath = (pathname) => {

    const { menuList } = this.state

    let path = {}

    menuList.forEach( item => {

      if (!item.children&&item.key === pathname) {

        path = {title:item.title}

      }else if(item.children){

        let findItme = item.children.find((vlaue)=>{

            return vlaue.key === pathname

        })

        if (findItme) {

          path = {title:item.title,children:findItme.title}

        }

      }

    })

    return path

  }

  UNSAFE_componentWillMount(){

    const { menuList } = this.state

    const pathname = this.props.location.pathname

    const { findPathName } = this.props

    let path = this.findHeadPath(pathname)

    findPathName(path)

    this.menu = this.getMenuNodes(menuList,pathname)

  }

  menuClick = ( item ) => {

    const { findPathName } = this.props

    let path = this.findHeadPath(item.key)

    findPathName(path)

  }

  render() {

    const  pathname  = this.props.location.pathname

    return (

      <div className="menu">

          <Menu theme="dark" 

          selectedKeys={[pathname]}

          defaultOpenKeys={[this.openKey]}
          
          mode="inline">

              {this.menu}

          </Menu>

      </div>
     
    );
  }

}

export default withRouter(menu)
