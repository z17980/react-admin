/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState,useEffect } from 'react';

import { Table, Input, Popconfirm, Form, Typography,Button } from '@/plugins/antd';

import '@/assets/style/table.less'

const EditableCell = ({
  editing,
  dataIndex,
  title,
  record,
  index,
  children,
  ...restProps
}) => {
  const inputNode = <Input/>;
  return (
    <td {...restProps}>
      {editing ? 
      (<Form.Item
          name={dataIndex}
          style={{
            margin: 0,
          }}
          rules={[
            {
              required: true,
              message: `Please Input ${title}!`,
            },
          ]}
        >
          {inputNode}
        </Form.Item>) : ( children )
      }
    </td>
  );
};

const EditableTable = (props) => {

  const { del,save,showChild,childSave,childDel } = props

  const [form] = Form.useForm();

  const [data, setData] = useState();

  const [catheInput,setCatheInput] = useState();

  useEffect(()=>{

    setData(props.table)

  }, [props.table])

  const [editingKey, setEditingKey] = useState('');

  const isEditing = (record) => record.key === editingKey;

  const tableDel = (id) => {

    del(id)

  }

  const childTableDel = (id) => {

    childDel(id)

  }

  const edit = (record) => {
    form.setFieldsValue({
      name: '',
      ...record,
    });
    setCatheInput(record.name)
    setEditingKey(record.key);
  };

  const cancel = () => {
    setEditingKey('');
  };

  const tableSave = async (record) => {

    const row = await form.validateFields();

    if (catheInput === row.name) {

      setEditingKey('');

      return

    }

    save(record.key,row.name)

    setEditingKey('');
    
  };

  const childTableSave = async (record) => {

    const row = await form.validateFields();

    if (catheInput === row.name) {

      setEditingKey('');

      return

    }

    childSave(record.key,row.name)

    setEditingKey('');
    
  };

  const child = (record) => {

    showChild(record.key,record.name)

  }

  const columns = [
    {
      title: '分类名称',
      dataIndex: 'name',
      width: '75%',
      editable: true,
    },
    {
      title: '操作',
      dataIndex: 'operation',
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <span>

            {
              
              record.pid === 0?
              <Button type="link" onClick={() => tableSave(record)}>保存</Button>:
              <Button type="link" onClick={() => childTableSave(record)}>保存</Button>
            
            }

            <Popconfirm title="确定取消?" onConfirm={cancel}>
              <a>取消</a>
            </Popconfirm>
            
          </span>
        ) : (
          <>

          <Typography.Link style={{
                marginRight: 15,
              }} disabled={editingKey !== ''} onClick={() => edit(record)}>
            修改分类
          </Typography.Link>

          {

            record.pid === 0?<Typography.Link style={{
                marginRight: 15,
              }} disabled={editingKey !== ''} onClick={() => child(record)}>
                查看子分类
            </Typography.Link>:<></>

          }

          {

            record.pid ===0?<Popconfirm title="确定删除?" onConfirm={()=>tableDel(record.key)}>
              <a>删除分类</a>
            </Popconfirm>:<Popconfirm title="确定删除?" onConfirm={()=>childTableDel(record.key)}>
              <a>删除子分类</a>
            </Popconfirm>

          }

          

          </>
        );
      },
    },
  ];

  const mergedColumns = columns.map((col) => {

    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    }

  });

  return (

    <>

    <Form form={form} component={false}>

      <Table

        rowClassName="editable-row"

        components={{
          body: {
            cell: EditableCell,
          }

        }}

        bordered

        dataSource={data}

        columns={mergedColumns}

        scroll={{ y: 560 }}

        pagination={{
          defaultPageSize:10,
          onChange: cancel,
        }}

      />

    </Form>

    </>

  );

};

export default EditableTable