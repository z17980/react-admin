import React, { Component } from 'react'

import {

  Button,

  Modal

} from '@/plugins/antd'

import './head.less'

// 工具

import { dateFormat } from '@/utils/util/time'

// api

// import { weather } from '@/api/home/weather'
import { withRouter } from 'react-router-dom'

class Head extends Component {

  state={

    time:'',

    visible:false,
    
    confirmLoading:false,

    cancelText:'取消',

    okText:'确认',
    
    modalText:'你确定退出登录吗?'

  }

  async findWeather(){

    // let result = await weather()

    // console.log(result);

  }

  handleOk = ()=>{

    this.setState({confirmLoading:true},()=>{

      localStorage.removeItem('token')

      localStorage.removeItem('user')

      this.props.history.replace('/login')

    })
    
  }

  handleCancel = ()=>{

    this.setState({visible:false})

  }

  findNowTime = ()=> {

    this.interva = setInterval(()=>{

      let time = dateFormat('YYYY-mm-dd HH:MM:SS',new Date())

      this.setState({time})

    },1000)

  }

  loginOut = ()=>{

    this.setState({visible:true})

  }

  componentWillUnmount(){

    clearInterval(this.interva)

    this.setState({interva:null})

  }

  componentDidMount(){

    this.findWeather()

    this.findNowTime()

  }

  render() {

    const { name,path } = this.props

    const { time,visible,confirmLoading,modalText,cancelText,okText } = this.state

    return (


      <div className="head">

          <div className="path">{path}</div>

          <div>

              <span className="time">{time}</span>

              <span>欢迎</span>{ name }

              <Button onClick={this.loginOut} danger type="text">
                  退出
              </Button>

          </div>

          <Modal
            title="退出登录！"
            visible={visible}
            confirmLoading={confirmLoading}
            okText={okText}
            cancelText={cancelText}
            centered={true}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
          >
            <p>{modalText}</p>
          </Modal>

      </div>

    )
  }
}

export default withRouter(Head)