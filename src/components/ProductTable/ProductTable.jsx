import React, { useState,useEffect } from 'react';
import { Table, Input, Popconfirm, Form, Typography,Button,Switch } from '@/plugins/antd';


const EditableCell = ({
  editing,
  dataIndex,
  title,
  record,
  index,
  children,
  ...restProps
}) => {
  const inputNode = <Input/>;
  return (
    <td {...restProps}>
      {editing ? 
      (<Form.Item
          name={dataIndex}
          style={{
            margin: 0,
          }}
          rules={[
            {
              required: true,
              message: `Please Input ${title}!`,
            },
          ]}
        >
          {inputNode}
        </Form.Item>) : ( children )
      }
    </td>
  );
};

const EditableTable = (props) => {

  const deleted = props.del

  const editRow = props.edit

  const change = props.change

  const detail = props.detail

  const [form] = Form.useForm();
  const [data, setData] = useState();
  const [editingKey, setEditingKey] = useState('');

  const isEditing = (record) => record.key === editingKey;

  useEffect(()=>{

    setData(props.data)

  }, [props.data])

  const edit = (record) => {
    form.setFieldsValue({
      name: '',
      age: '',
      address: '',
      ...record,
    });
    setEditingKey(record.key);
  };

  const cancel = () => {
    setEditingKey('');
  };

  const save = async (key) => {
    try {
      const row = await form.validateFields();
      editRow(key,row)
      setEditingKey('');
    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };

  const del = (id) => {

    deleted(id)

  }

  const onChange = (id) => {

    change(id)

  }

  const columns = [
    {
      title: '商品名称',
      dataIndex: 'name',
      width: '25%',
      editable: true,
    },
    {
      title: '商品描述',
      dataIndex: 'info',
      width: '40%',
      editable: true,
    },
    {
      title: '价格',
      dataIndex: 'price',
      width: '8%',
      editable: true,
    },
    {
      title: '状态',
      dataIndex: 'status',
      width: '15%',
      render: (_, record) => {
        return(
           <Switch onChange={()=>{onChange(record.key)}} defaultChecked={record.status===1}></Switch>
        );
      }
    },
    {
      title: '操作',
      dataIndex: 'operation',
      render: (_, record) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <Button type="link"
              onClick={() => save(record.key)}
              style={{
                marginRight: 8,
              }}
            >
              保存
            </Button>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              取消
            </Popconfirm>
          </span>
        ) : (
          <>
          <Typography.Link style={{
                marginRight: 15,
              }} onClick={() => detail(record)}>
            详情
          </Typography.Link>
          <Typography.Link disabled={editingKey !== ''} style={{
                marginRight: 15,
              }} onClick={() => edit(record)}>
            编辑
          </Typography.Link>
            <Popconfirm title="确定删除?" onConfirm={()=>del(record.key)}>
              <Button type="link">删除</Button>
            </Popconfirm>
          </>
        );
      },
    },
  ];

  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: (record) => ({
        record,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });
  return (
    <Form form={form} component={false}>
      <Table
        components={{
          body: {
            cell: EditableCell,
          },
        }}
        bordered
        dataSource={data}
        columns={mergedColumns}
        rowClassName="editable-row"
        scroll={{ y: 560 }}
        pagination={{
          defaultPageSize:10,
          onChange: cancel,
        }}
      />
    </Form>
  );
};

export default EditableTable