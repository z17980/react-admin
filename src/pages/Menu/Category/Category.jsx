import React, { Component } from 'react'

import Table from '@/components/Table/Table'

import { 
  categoryFirst,
  addCategoryFirst,
  addCategoryTwo,
  delCategoryFirst,
  saveCategoryFirst,
  findTwoCategories,
  saveChildCategory,
  delChildCategory
} from '@/api/product/category'

import { 
  
  Input,
  
  Button,
  
  PlusOutlined,Breadcrumb,
  
  Modal,Select,

  message

  } from '@/plugins/antd'

const { Option } = Select;

export default class Category extends Component {

  state = {

    isModalVisible:false,

    grade:1,

    gradeToAdd:1,

    addText:'',

    addTwoId:null,

    childTitle:'',

    currentPid:null,

    data: []

  }

  add = () => {

    this.setState({isModalVisible:true})

  }

  del = async (id) => {

    let result = await delCategoryFirst({

      id

    })

    if (result.code === 200) {
      
      message.success(result.msg)

      this.findCategoryFirst()

    }

  }

  childDel = async (id) => {

    const { currentPid,childTitle } = this.state

    let result = await delChildCategory({

      id

    })

    if (result.code === 200) {
      
      message.success(result.msg)

      this.child(currentPid,childTitle)

    }

  }

  save = async (id,name) => {

    let result = await saveCategoryFirst({

      id,name

    })

    if (result.code === 200) {
      
      message.success(result.msg)

      this.findCategoryFirst()

    }

  }

  childSave = async (id,name) => {

    const { currentPid,childTitle } = this.state

    let result = await saveChildCategory({

      id,name

    })

    if (result.code === 200) {
      
      message.success(result.msg)

      this.child(currentPid,childTitle)

    }

  }

  child = async (pid,name) => {

    this.setState({childTitle:name})

    this.setState({currentPid:pid})

    let result = await findTwoCategories({

      pid

    })

    this.setState({data:result.data})

    this.setState({gradeToAdd:2})

  }

  back = async () => {

    this.findCategoryFirst()

    this.setState({childTitle:''})

    this.setState({gradeToAdd:1})

  }

  handleOk = async () => {

    const { addText,grade,addTwoId } = this.state

    if (addText.trim() === '') {
      
      message.error('请输入一级分类名称')

      return

    }

    this.setState({isModalVisible:false})

    if (grade === 1) {
      
        let result = await addCategoryFirst({
        
          name:addText
        
        })
      
        if (result.code === 200) {
        
          message.success(result.msg)
        
          this.findCategoryFirst()

        }
        else if( result.code ===203 ){
        
          message.error(result.msg)
        
        }
      
    }
    else if(grade === 2){

        let result = await addCategoryTwo({

          pid:addTwoId,

          name:addText

        })

        if (result.code === 200) {
        
          message.success(result.msg)
        
        }
        else if( result.code ===203 ){
        
          message.error(result.msg)
        
        }

    }

  }

  handleCancel = () => {

    this.setState({isModalVisible:false})
    
  }

  input = (e) =>{

    this.setState({addText:e.target.value})

  }

  findCategoryFirst = async () => {

    let reslut = await categoryFirst()

    this.setState({ data: reslut.data },()=>{

      const { data } = this.state

      try{

        this.setState({addTwoId:data[0].key})

      }catch{

      }

    })

  }

  selectChange = (e) => {

    this.setState({grade:e})

  }

  selectTwoChange = (e) => {

    this.setState({addTwoId:e})

  }

  componentDidMount() {

    this.findCategoryFirst()

  }

  render() {

    const { isModalVisible,data,childTitle,grade,gradeToAdd } = this.state

    let selectFirstName = ''

    try{

      selectFirstName = data[0].name

    }catch{

    }

    return (
      <>

        <div className="table-head">

          <Breadcrumb style={{ margin: '16px 0' }}>

            <Breadcrumb.Item onClick={this.back}>{'分类名称'}</Breadcrumb.Item>

            <Breadcrumb.Item>{ childTitle }</Breadcrumb.Item>

          </Breadcrumb>

          {

            gradeToAdd===2?<></>:<Button onClick={this.add} type="primary" style={{ marginBottom: 16 }}>

                          <PlusOutlined />添加

            </Button>

          }

        </div>

        <Table 
        
        table={data} 
        del={this.del} 
        save={this.save} 
        showChild={this.child} 
        childSave={this.childSave}
        childDel={this.childDel}
        ></Table>
        
        <Modal 

        title="添加分类" 

        visible={isModalVisible} 

        onOk={this.handleOk} 

        onCancel={this.handleCancel}

        centered={true}    

        >
          <Select defaultValue="一级分类" style={{ width:'100%',marginBottom: 20 }} onChange={this.selectChange}>
                  <Option value={1}>一级分类</Option>
                  <Option value={2}>二级分类</Option>
          </Select>

          {

            grade===2?<Select defaultValue={selectFirstName} style={{ width:'100%',marginBottom: 20 }} onChange={this.selectTwoChange}>
                  {

                    data.map(item => {

                      return <Option key={item.name} value={item.key}>{item.name}</Option>

                    })

                  }
            </Select>:<></>

          }

          <Input onChange={this.input} placeholder="分类名称" />

        </Modal>


      </>
    )

  }

}
