import React, { Component } from 'react'

import ProductTable from '@/components/ProductTable/ProductTable'

import { Button, PlusOutlined, Select, Input, Form, Modal, message } from '@/plugins/antd';

import '@/assets/style/table.less'

import './product.less'

import { add, find, del, edit, change, search, findByStatus } from '@/api/product/product'

const { Search } = Input;

const { Option } = Select;

export default class Product extends Component {

    state = {

        data: [],

        isModalVisible: false,

        initialValues: { name: '小米平板5', info: '1999惊爆价', price: 1999, status: '在售' },

        searchKey: 'byName',

        searchStatus: '0'

    }

    onSearch = async (val) => {

        const result = await search({

            searchStatus: this.state.searchStatus,

            searchKey: this.state.searchKey,

            inputKey: val

        })

        if (result.code === 200) {

            message.success(result.msg)

            this.setState({ data: result.data })

        }

    }

    add = () => {

        this.setState({ isModalVisible: true })

    }

    del = async (id) => {

        let result = await del({ id })

        if (result.code === 200) {

            message.success(result.msg)

            this.find()

        }

    }

    detail = async (row) => {

        this.props.history.push({
            pathname: '/detail',
            state: row
        })
    }

    edit = async (key, row) => {

        let result = await edit({

            key, row

        })

        if (result.code === 200) {

            message.success(result.msg)

            this.find()

        }

    }

    find = async () => {

        let result = await find()

        this.setState({ data: result.data })

    }

    change = async (key) => {

        let result = await change({ key })

        if (result.code === 200) {

            message.success(result.msg)

        }

    }

    handleOk = () => {

        this.setState({ isModalVisible: false })

    }

    handleCancel = () => {

        this.setState({ isModalVisible: false })

    }

    onFinish = async (val) => {

        let result = await add(val)

        if (result.code === 200) {

            message.success(result.msg)

            this.setState({ isModalVisible: false })

            this.find()

        }

    }

    componentDidMount() {

        this.find()

    }

    handleChange = (val) => {

        this.setState({

            searchKey: val

        }, () => {

            if (this.state.searchKey === 'byStatus') {

                this.handleStatusChange('1')

            }

        })

    }

    handleStatusChange = (val) => {

        this.setState({

            searchStatus: val

        }, async () => {
            
            this.find()

            const result = await findByStatus({

                searchStatus: this.state.searchStatus,

            })

            if (result.code === 200) {

                message.success(result.msg)

                this.setState({ data: result.data })

            }

        })

    }

    render() {

        const { data, isModalVisible, initialValues, searchKey } = this.state

        return (
            <>
                <div className="table-head">

                    <div className="search">
                        <div>
                            <Select defaultValue="byName" style={{ width: 150 }} allowClear onChange={this.handleChange}>
                                <Option value="byName">按名称搜索</Option>
                                <Option value="byPrice">按价格搜索</Option>
                                <Option value="byStatus">按状态搜索</Option>
                            </Select>
                        </div>

                        {

                            searchKey === 'byStatus' ? <Select defaultValue="1" style={{ width: 150 }} allowClear onChange={this.handleStatusChange}>
                                <Option value="1">在售</Option>
                                <Option value="0">下架</Option>
                            </Select> : <></>

                        }

                        <div style={{ marginLeft: 20 }}>

                            <Search
                                placeholder="请输入关键字"
                                allowClear
                                enterButton="搜索"
                                size="large"
                                onSearch={this.onSearch}
                            />

                        </div>
                    </div>

                    <Button onClick={this.find} type="primary" style={{ marginBottom: 16 }}>
                                重置
                            </Button>

                    <Button onClick={this.add} type="primary" style={{ marginBottom: 16 }}>

                        <PlusOutlined />添加商品

                    </Button>

                </div>

                <ProductTable data={data} del={this.del} edit={this.edit} change={this.change} detail={this.detail}></ProductTable>

                <Modal

                    title="添加商品"

                    visible={isModalVisible}

                    onOk={this.handleOk}

                    onCancel={this.handleCancel}

                    centered={true}

                    footer={null}

                >

                    <Form
                        name="normal_login"
                        className="login-form"
                        initialValues={initialValues}
                        onFinish={this.onFinish}
                    >

                        <Form.Item
                            name={['name']}
                            label=""
                            rules={[{ required: true }]}
                        >
                            <Input className="modal-input" placeholder="商品名称" />

                        </Form.Item>

                        <Form.Item
                            name={['info']}
                            label=""
                        >
                            <Input className="modal-input" placeholder="商品描述" />

                        </Form.Item>

                        <Form.Item
                            name={['price']}
                            label=""
                            rules={[{ required: true }, { pattern: /^\d+(\.\d+)?$/, message: '请输入数字!' },]}
                        >
                            <Input className="modal-input" onChange={this.input} placeholder="商品价格" />

                        </Form.Item>

                        <Form.Item
                            name={['status']}
                            label=""
                            rules={[{ required: true }]}
                        >

                            <Select style={{ width: '100%', marginBottom: 20 }}>
                                <Option value={1}>在售</Option>
                                <Option value={0}>下架</Option>
                            </Select>

                        </Form.Item>

                        <Form.Item className="form-btn">

                            <Button type="info" onClick={this.handleCancel}>取消</Button>

                            <Button type="primary" htmlType="submit" className="login-form-button">
                                确定添加
                            </Button>

                        </Form.Item>

                    </Form>

                </Modal>
                        
            </>
        )
    }

}
