import React, { Component } from 'react'

import './login.less'

import {

  Button,Form, Input, Checkbox,

  SlackSquareFilled,UserOutlined, LockOutlined, message
  
} from '@/plugins/antd'

// 接口

import { login } from '@/api/user/login'

export default class Login extends Component {

  state={

      remember:false,

      initialValues:{ username:'admin',password:'',remember: true },

      userRules:[
        { required: true, message: '请输入你的用户名!' },
        { min: 4, message: '用户名最少4位!' },
        { max: 12, message: '用户名最多12位!' },
        { pattern: /^[a-zA-Z0-9_]+$/, message: '用户名必须是英文、数字或者下划线组合!' },
      ],

      pwdRules:[
        { required: true, message: '请输入你的密码!' },
        { min: 4, message: '密码最少4位!' },
        { max: 12, message: '密码最多12位!' },
        { pattern: /^[a-zA-Z0-9_]+$/, message: '密码必须是英文、数字或者下划线组合!' },
      ]

  }

  onFinish = async (values) => {

    let { username,password,remember } = values

    let res = await login({

      username,

      password

    })

    if(res.code === 200){

      message.success(res.msg)

      if (remember) {

        localStorage.setItem('pwd',password)
  
      }

      localStorage.setItem('user',JSON.stringify({name:res.data.name,role:res.data.role}))

      localStorage.setItem('token',res.token)

      this.props.history.replace('/home')

    }

  }

  componentDidMount(){

    let pwd = localStorage.getItem('pwd')

    if (pwd) {
      
      this.setState({initialValues:{ username:'admin',password:pwd,remember: true }},()=>{

      })

    }

  }

  render() {

    const { initialValues,userRules,pwdRules } = this.state

    return (
      
      <div className="login">

        <header>
          <SlackSquareFilled />
          <span className="title">React管理系统</span>
        </header>

        <main>

          <div className="title">用户登录</div>

          <Form
            name="normal_login"
            className="login-form"
            initialValues={initialValues}
            onFinish={this.onFinish}
          > 

            <Form.Item
              name="username"
              rules={userRules}
            >

              <Input 
              
              prefix={<UserOutlined className="site-form-item-icon" />} 
              
              placeholder="用户名" 

              />

            </Form.Item>

            <Form.Item
              name="password"
              rules={pwdRules}
            >

              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="密码"
              />

            </Form.Item>

            <Form.Item>

              <Form.Item name="remember" valuePropName="checked" noStyle>
                <Checkbox>记住我</Checkbox>
              </Form.Item>

            </Form.Item>

            <Form.Item>

              <Button type="primary" htmlType="submit" className="login-form-button">
                登录
              </Button>

            </Form.Item>

          </Form>

        </main>

      </div>
    )
  }

}
