import React, { Component } from 'react'

import {

  Layout,

  Breadcrumb,

  SlackSquareFilled

} from '@/plugins/antd'

import './home.less'

import Menu from '@/components/Menu/menu'
import Head from '@/components/Head/Head'

import { Link, Redirect, Route, Switch } from 'react-router-dom'

import Index from '@/pages/Menu/Index/Index'
import Product from '@/pages/Menu/Product/Product'
import Detail from '@/pages/Menu/Product/Detail'
import Category from '@/pages/Menu/Category/Category'
import User from '@/pages/Menu/User/User'
import Role from '@/pages/Menu/Role/Role'
import Bar from '@/pages/Menu/Charts/Bar'
import Line from '@/pages/Menu/Charts/Line'
import Pie from '@/pages/Menu/Charts/Pie'


const { Header, Footer, Sider, Content } = Layout

export default class Home extends Component {

  state = {

    collapsed: false,

    path:{title:'首页'}

  }

  onCollapse = collapsed => {
    this.setState({ collapsed });
  }

  findPathName = (path)=> {

      this.setState({path})

  }

  render() {

    let token = localStorage.getItem('token')

    if(!token){

      return (<Redirect to='/login'></Redirect>)

    }

    const { collapsed,path } = this.state;

    return (

      <div className="home">

        <Layout style={{ minHeight: '100vh' }}>

          <Sider width={300} collapsible collapsed={collapsed} onCollapse={this.onCollapse}>

            <Link to="/" className={collapsed?"logo iscoll":'logo'}>
                <SlackSquareFilled />
                {!collapsed?<span className="title">React管理系统</span>:null}
            </Link>

            <Menu findPathName={this.findPathName}></Menu>

          </Sider>

          <Layout className="site-layout">

            <Header style={{ background:'#fff' }}>

                <Head name="admin"  path={path.title}></Head>

            </Header>

            <Content style={{ margin: '0 16px' }}>

              <Breadcrumb style={{ margin: '16px 0' }}>

                <Breadcrumb.Item>{path.children?path.title:''}</Breadcrumb.Item>

                <Breadcrumb.Item>{path.children}</Breadcrumb.Item>

              </Breadcrumb>

              <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>

                <Switch>

                    <Route path="/index" component={Index}></Route>
                    <Route path="/product" component={Product}></Route>
                    <Route path="/detail" component={Detail}></Route>
                    <Route path="/category" component={Category}></Route>
                    <Route path="/user" component={User}></Route>
                    <Route path="/role" component={Role}></Route>
                    <Route path="/charts/bar" component={Bar}></Route>
                    <Route path="/charts/line" component={Line}></Route>
                    <Route path="/charts/pie" component={Pie}></Route>

                    <Redirect to="/index" />

                </Switch>

              </div>

            </Content>

            <Footer style={{ textAlign: 'center' }}> <SlackSquareFilled /> Ant Design React 管理系统</Footer>

          </Layout>

        </Layout>

      </div>

    )
  }

}
