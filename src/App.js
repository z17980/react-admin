import React, { Component } from 'react'

import { Switch,Route } from 'react-router-dom'

import './App.less'

import Login from './pages/Login/login'

import Home from './pages/Home/home'

export default class App extends Component {

  render() {

    return (

      <div className="app">

        <Switch>

          <Route path="/login" component={Login}></Route>

          <Route path="/" component={Home}></Route>

        </Switch>

      </div>

    )

  }

}

