import request from '@/utils/reques'

const add = (data) => {

    return request({

        url:'/product/add',

        method: 'post',

        data

    })

}

const find = (data) => {

    return request({

        url:'/product/find',

        method: 'post',

        data

    })

}

const del = (data) => {

    return request({

        url:'/product/del',

        method: 'post',

        data

    })

}

const edit = (data) => {

    return request({

        url:'/product/edit',

        method: 'post',

        data

    })

}

const change = (data) => {

    return request({

        url:'/product/change',

        method: 'post',

        data

    })

}

const search = (data) => {

    return request({

        url:'/product/search',

        method: 'post',

        data

    })

}

const findByStatus = (data) => {

    return request({

        url:'/product/findByStatus',

        method: 'post',

        data

    })

}



export {

    add,

    find,

    del,

    edit,

    change,

    search,

    findByStatus

}