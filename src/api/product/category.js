import request from '@/utils/reques'

const categoryFirst = (data)=>{

    return request({

      url: '/product/categoryFirst',

      method: 'post',

      data
      
    })

}

const addCategoryFirst = (data)=>{

  return request({

    url: '/product/addCategoryFirst',

    method: 'post',

    data
    
  })

}

const addCategoryTwo = (data)=>{

  return request({

    url: '/product/addCategoryTwo',

    method: 'post',

    data
    
  })

}

const delCategoryFirst = (data)=>{

  return request({

    url: '/product/delCategoryFirst',

    method: 'post',

    data
    
  })

}

const saveCategoryFirst = (data)=>{

  return request({

    url: '/product/saveCategoryFirst',

    method: 'post',

    data
    
  })

}

const findTwoCategories = (data)=>{

  return request({

    url: '/product/findTwoCategories',

    method: 'post',

    data
    
  })

}

const saveChildCategory = (data)=>{

  return request({

    url: '/product/saveChildCategory',

    method: 'post',

    data
    
  })

}

const delChildCategory = (data)=>{

  return request({

    url: '/product/delChildCategory',

    method: 'post',

    data
    
  })

}

export {

  categoryFirst,

  addCategoryFirst,

  addCategoryTwo,

  delCategoryFirst,

  saveCategoryFirst,

  findTwoCategories,

  saveChildCategory,

  delChildCategory

}