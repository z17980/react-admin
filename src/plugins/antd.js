
// 组件
import { 
  message,
  // 布局
  Layout,Breadcrumb,
  // 菜单
  Menu,
  // 提示
  Button,Alert,Modal,
  // 表单
  Form, Input, Checkbox,InputNumber,Select,
  // 表格
  Table, Popconfirm,Typography,

  Switch
} from 'antd';

// 图标组件
import { 
  SlackSquareFilled,UserOutlined,LockOutlined,
  ProfileOutlined,DesktopOutlined,PieChartOutlined,BarChartOutlined,TeamOutlined,
  createFromIconfontCN,
  PlusOutlined
  } from '@ant-design/icons';

export {
  
  message,

  Layout,Breadcrumb,

  Menu,

  Button,Alert,Modal,
  
  Form, Input, Checkbox,InputNumber,Select,

  Table, Popconfirm,Typography,

  Switch,

  SlackSquareFilled,UserOutlined, LockOutlined,

  ProfileOutlined,DesktopOutlined,PieChartOutlined,BarChartOutlined,TeamOutlined,

  createFromIconfontCN,

  PlusOutlined

}