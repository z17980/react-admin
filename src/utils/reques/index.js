import axios from 'axios'

import { message } from '@/plugins/antd'

import { token } from '@/utils/token'

import err from '@/utils/err'

axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8'
// 创建axios实例
const service = axios.create({

  baseURL: process.env.VUE_APP_BASE_API||'http://localhost:5200/',
  // 超时
  timeout: 10000
})
// request拦截器
service.interceptors.request.use(config => {

  // 是否需要设置 token
  const isToken = (config.headers || {}).isToken === false

  if (token() && !isToken) {
    config.headers['Authorization'] = 'Bearer ' + token() // 让每个请求携带自定义token 请根据实际情况自行修改
  }
  // get请求映射params参数
  if (config.method === 'get' && config.params) {
    let url = config.url + '?';

    for (const propName of Object.keys(config.params)) {
      const value = config.params[propName];
      var part = encodeURIComponent(propName) + "=";
      if (value && typeof(value) !== "undefined") {
        if (typeof value === 'object') {
          for (const key of Object.keys(value)) {
            let params = propName + '[' + key + ']';
            var subPart = encodeURIComponent(params) + "=";
            url += subPart + encodeURIComponent(value[key]) + "&";
          }
        } else {
          url += part + encodeURIComponent(value) + "&";
        }
      }
    }
    
    url = url.slice(0, -1);
    config.params = {};
    config.url = url;
  }
  return config
}, error => {
    Promise.reject(error)
})

// 响应拦截器
service.interceptors.response.use(res => {
    // 未设置状态码则默认成功状态
    const code = res.data.code || 200;
    // 获取错误信息
    const msg = err[code] || res.data.msg || err['default']

    if (code === 401) {

      message.error('登录状态已过期,请重新登录',0,()=>{

        // redux重置登录状态

      })

    } else if (code === 500) {

      message.error(msg)
      return Promise.reject(msg)

    } else if (code !== 200) {

      message.error(msg)
      return Promise.reject(msg)

    } else {

      return res.data

    }
    
  },
  error => {

    return Promise.reject(error)

  }

)

export default service
